//
//  ROCeaserCrypto.m
//  CeaserCrypto
//
//  Created by apple on 08.03.16.
//  Copyright © 2016 Ostap Romanko. All rights reserved.
//

#import "ROCeaserCrypto.h"

static NSString * const kEnglishAlphabetUppercaseCharacterSet = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static NSString * const kEnglishAlphabetLowercaseCharacterSet = @"abcdefghijklmnopqrstuvwxyz";

static NSString * const kUkrainianAlphabetUppercaseCharacterSet = @"АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ";
static NSString * const kUkrainianAlphabetLowercaseCharacterSet = @"абвгґдеєжзиіїйклмнопрстуфхцчшщьюя";

@implementation ROCeaserCrypto

+ (NSString *)encodeString:(NSString *)plainString withStep:(NSInteger)step {
    unichar lettersBuffer[plainString.length];
    
    [plainString getCharacters:lettersBuffer range:NSMakeRange(0, plainString.length)];
    
    for (int i = 0; i < plainString.length; i++) {
        unichar letter = lettersBuffer[i];
        lettersBuffer[i] = [ROCeaserCrypto shiftedLetter:letter withStep:step];
    }
    
    return [NSString stringWithCharacters:lettersBuffer length:plainString.length];
}

+ (unichar)shiftedLetter:(unichar)letter withStep:(NSInteger)step {
    for (NSString *alphabet in [ROCeaserCrypto supportedAlphabets]) {
        NSInteger letterPosition = [alphabet rangeOfString:[NSString stringWithCharacters:&letter length:1]].location;
        if (letterPosition != NSNotFound) {
            NSInteger shiftedPosition = letterPosition + step;
            if (shiftedPosition >= alphabet.length) {
                shiftedPosition %= alphabet.length;
            }
            
            return [alphabet characterAtIndex:shiftedPosition];
        }
    }
    return letter;
}

+ (NSArray <NSString *> *)supportedAlphabets {
    static NSArray <NSString *> *alphabets = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        alphabets = @[kEnglishAlphabetUppercaseCharacterSet,
                      kEnglishAlphabetLowercaseCharacterSet,
                      kUkrainianAlphabetUppercaseCharacterSet,
                      kUkrainianAlphabetLowercaseCharacterSet];
    });
    
    return alphabets;
}

@end
