//
//  ROCeaserCrypto.h
//  CeaserCrypto
//
//  Created by apple on 08.03.16.
//  Copyright © 2016 Ostap Romanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ROCeaserCrypto : NSObject

/**
 *  Encodes string by using Ceaser cipher with custom shift.
 *
 *  @param plainString String to encrypt.
 *  @param step        Step to shift every letter in string.
 *
 *  @return Encoded string.
 */
+ (NSString *)encodeString:(NSString *)plainString withStep:(NSInteger)step;

@end
