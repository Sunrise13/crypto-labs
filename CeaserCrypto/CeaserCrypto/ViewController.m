//
//  ViewController.m
//  CeaserCrypto
//
//  Created by apple on 27.02.16.
//  Copyright © 2016 Ostap Romanko. All rights reserved.
//

#import "ViewController.h"

#import "ROCeaserCrypto.h"

@interface ViewController () <NSTextViewDelegate>

@property (unsafe_unretained) IBOutlet NSTextView *plainTextView;
@property (unsafe_unretained) IBOutlet NSTextView *cipherTextView;

@property (weak) IBOutlet NSTextFieldCell *stepTextFieldCell;


@property (assign) NSUInteger step;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.plainTextView.delegate = self;
    
    self.step = 24;
    
    [self addObserver:self forKeyPath:@"step" options:NSKeyValueObservingOptionNew context:NULL];

    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)updateCipher {
    NSString *encodedString = [ROCeaserCrypto encodeString:self.plainTextView.textStorage.string withStep:self.step];
    NSTextStorage * storage = [[NSTextStorage alloc] initWithString:encodedString];
    
    [self.cipherTextView.layoutManager replaceTextStorage:storage];
}

#pragma mark - NSTextViewDelegate

- (void)textDidChange:(NSNotification *)notification {
    [self updateCipher];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"step"]) {
        [self updateCipher];
    }
}

#pragma mark - Dealloc

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"step"];
}

@end
