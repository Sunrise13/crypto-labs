//
//  main.m
//  CeaserCrypto
//
//  Created by apple on 27.02.16.
//  Copyright © 2016 Ostap Romanko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
